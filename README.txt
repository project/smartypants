smartypants.module
README.txt

The smartypants filter module uses SmartyPants-PHP to translate plain
ASCII punctuation characters into "smart" typographic punctuation HTML
entities. It performs the following transformations:

* Straight quotes ( " and ' ) into "curly" quote HTML entities
* Backticks-style quotes (``like this'') into "curly" quote HTML
  entities
* Dashes ("--"; and "---") into en- and em-dash entities
* Three consecutive dots ("...") into an ellipsis entity

More information about SmartyPants can be found at the SmartyPants
home page <http://daringfireball.net/projects/smartypants/> or
SmartyPants-PHP home page <http://monauraljerk.org/smartypants-php/>.

Files
  - smartypants.module
      the actual module (PHP source code)

  - smartypants.info
      the module information file used by Drupal

  - smartypants-php (directory)
      a PHP port of John Gruber's (<http://daringfireball.net/>) Perl
      SmartyPants.pl Movable Type plugin ported by Matthew McGlynn
      (PHP source code); this file is available separately under a
      custom redistribution license from
      <http://monauraljerk.org/smartypants-php/>

  - README.txt (this file)
      general module information

  - INSTALL.txt
      installation/configuration instructions

  - CREDITS.txt
      information on those responsible for this module

  - TODO.txt
      feature requests and modification suggestions

  - CHANGELOG.txt
      change/release history for this module
